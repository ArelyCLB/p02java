package com.example.practica02imc;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private EditText lblPeso;
    private EditText lblAltura;
    private TextView txtRes;
    private Button btnCalcular;
    private Button btnLimpiar;
    private Button btnCerrar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        lblPeso = findViewById(R.id.lblPeso);
        lblAltura = findViewById(R.id.lblAltura);
        txtRes = findViewById(R.id.txtRes);
        btnCalcular = findViewById(R.id.btnCalcular);
        btnLimpiar = findViewById(R.id.btnLimpiar);
        btnCerrar = findViewById(R.id.btnCerrar);

        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calcularIMC();
            }
        });
        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lblPeso.setText("");
                lblAltura.setText("");
                txtRes.setText("");
            }
        });

        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void calcularIMC() {
        String txtPeso = lblPeso.getText().toString();
        String txtAltura = lblAltura.getText().toString();

        if (txtPeso.isEmpty() || txtAltura.isEmpty()) {
            txtRes.setText("Ingresa el peso y altura: ");
            return;
        }

        float peso = Float.parseFloat(txtPeso);
        float altura = Float.parseFloat(txtAltura);

        float imc = peso / (altura * altura);

        String txtResu = "Su IMC es: " + imc;

        if (imc < 18.5) {
            txtResu += "\nBajo peso";
        } else if (imc < 25) {
            txtResu += "\nPeso normal";
        } else if (imc < 30) {
            txtResu += "\nSobrepeso";
        } else {
            txtResu += "\nObesidad";
        }

        txtRes.setText(txtResu);
    }
}
